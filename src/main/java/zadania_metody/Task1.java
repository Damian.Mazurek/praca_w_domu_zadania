package zadania_metody;

import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Podaj pierwszą liczbę: ");
        int a = in.nextInt();
        System.out.println("Podaj drugą liczbę: ");
        int b = in.nextInt();

        Methods multi = new Methods();
        System.out.println("Wynik mnożenia liczby "+a+" i "+b+" wynosi "+multi.multiply(a, b));

    }

    static class Methods
    {
        public int multiply (int a, int b){
            return a*b;
        }
    }
}
