package zadania_metody;

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {

        try {
            Scanner in = new Scanner(System.in);
            System.out.println("Podaj liczbę a: ");
            int a = in.nextInt();
            System.out.println("Podaj liczbę b: ");
            int b = in.nextInt();

            System.out.println("Iloraz liczb " + a + " i " + b + " wynosi " + divide(a, b));
        } catch (ArithmeticException e) {
            System.out.println("Nie wolno dzielić przez 0");
        }

    }

    public static int divide(int a, int b) {
        return a / b;
    }
}
