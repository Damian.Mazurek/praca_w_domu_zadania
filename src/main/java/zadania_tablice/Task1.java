package zadania_tablice;

public class Task1 {

    public static int firstElement(int[]tab){
        return tab[0];
    }

    public static void main(String[] args) {
        int [] array = {1,2,3,4};
        int [] newArray = {4,9,12,1};

        System.out.println(firstElement(array));
        System.out.println(firstElement(newArray));
    }
}
