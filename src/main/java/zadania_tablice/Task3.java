package zadania_tablice;

public class Task3 {

    public static int arraySum (int [] tab){
        return tab[0]+tab[1];
    }

    public static void main(String[] args) {
        int [] array = {1,2};
        int [] array2 ={4,8};

        System.out.println(arraySum(array));
        System.out.println(arraySum(array2));
    }
}
