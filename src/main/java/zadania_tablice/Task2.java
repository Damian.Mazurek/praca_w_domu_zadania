package zadania_tablice;

public class Task2 {

    public static int lastElement(int[]tab){
        return tab[tab.length-1];
    }

    public static void main(String[] args) {
        int [] array = {1,2,3,4};
        int [] newArray = {4,9,12,1};

        System.out.println(lastElement(array));
        System.out.println(lastElement(newArray));
    }
}

