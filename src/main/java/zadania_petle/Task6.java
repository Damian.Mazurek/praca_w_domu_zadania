package zadania_petle;

import java.util.Scanner;

public class Task6 {
    public static void main(String[] args) {

        System.out.print("Wprowadź liczbę do obliczenia silni: ");
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();

        int silnia = 1;

        for (int i = 1; i <= n; i++) {
            silnia = silnia * i;
        }
        System.out.println(n+"! = "+silnia);
    }
}
