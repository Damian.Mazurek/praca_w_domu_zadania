package zadania_petle;

import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {

        String haslo = "Polska";
        String wprowadzoneHaslo;
        do{
            System.out.print("Podaj hasło: ");
            Scanner in = new Scanner(System.in);
            wprowadzoneHaslo =in.nextLine();
            if(!haslo.equals(wprowadzoneHaslo)){
                System.out.println("Wprowadź poprawne hasło!");
            }
        }while(!haslo.equals(wprowadzoneHaslo));

        System.out.println("Witaj, wprowadzono poprawne hasło :)");
    }
}
