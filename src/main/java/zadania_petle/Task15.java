package zadania_petle;

import java.util.Scanner;

public class Task15 {

    public static int dzielnik;

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        System.out.println("Podaj pierwszą liczbę: ");
        int number = in.nextInt();

        System.out.println("Podaj druga liczbę: ");
        int number2 = in.nextInt();


        if (number != 0 && number2 != 0) {
            for (int i = 1; i <= number; i++) {
                if (number % i == 0 && number2 % i == 0) {
                    dzielnik = i;
                }
            }
            System.out.println("Najwiekszy wspólny dzielnik liczb "
                    +number+" i "+number2+" wynosi "+dzielnik);
        }else{
            System.out.println("Dane liczby nie mają wspólnych dzielników");
        }
    }
}
