package zadania_petle;

import java.util.Scanner;

public class Task8 {
    public static void main(String[] args) {

        System.out.println("Podaj słowo do sprawdzenia: ");

        Scanner in = new Scanner(System.in);
        String word = in.nextLine();

        char [] array = word.toCharArray();

        System.out.println("Cyfry zawarte w danym słowie to:");
        for (char ch : array)
              {
            if(Character.isDigit(ch))
            {
                System.out.print(ch+"\t");
            }
        }
    }
}
