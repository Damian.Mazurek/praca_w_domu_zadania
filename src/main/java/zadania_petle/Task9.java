package zadania_petle;

import java.util.Scanner;

public class Task9 {
    public static void main(String[] args) {
        System.out.println("Podaj wyraz do sprawdzenia czy jest palindromem: ");

        Scanner in = new Scanner(System.in);
        String word = in.nextLine();
        StringBuilder word2 = new StringBuilder();

        for (int i = word.length() - 1; i >= 0; i--) {
            word2.append(word.charAt(i));
        }
        if (word.equalsIgnoreCase(word2.toString())) {
            System.out.println("Słowo " + word + " jest palindromem");
        } else {
            System.out.println("Słowo " + word + " nie jest palindromem");
        }
    }
}
