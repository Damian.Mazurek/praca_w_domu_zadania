package zadania_petle;

import java.util.Scanner;

public class Task11 {
    public static void main(String[] args) {

        System.out.println("Podaj liczbę do sprawdzenia: ");
        Scanner in = new Scanner(System.in);
        int number = in.nextInt();
        boolean liczbaPierwsza = true;

        for (int i = 2; i < number; i++) {
            if (number % i == 0) {
                liczbaPierwsza = false;
                break;
            }
        }
        if (liczbaPierwsza){
            System.out.println("Liczba jest pierwsza");
        }else{
            System.out.println("Liczba jest złożona");
        }
    }
}
