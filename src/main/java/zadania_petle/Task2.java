package zadania_petle;

import java.util.Scanner;

public class Task2
{
    public static void main(String[] args)
    {
        System.out.println("Podaj liczbę do zsumowania: ");
        Scanner in = new Scanner(System.in);
        int number = in.nextInt();
        int suma=0;

        if(number==0)
        {
            System.out.println("Suma liczb od 1 do " + number + " wynosi 1");

        }else if (number<0)
        {
            System.out.println("Pętla nie może wykonać się ujemną ilość razy");
        }else
            {

                for (int i = 1; i <= number; i++)
                {
                    System.out.print(suma + "+" + i + "=");
                    suma += i;
                    System.out.println(suma);
                }

                System.out.println("Suma liczb od 1 do " + number + " wynosi " + suma);
            }

        }
    }
