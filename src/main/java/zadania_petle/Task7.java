package zadania_petle;

public class Task7 {
    public static void main(String[] args) {

        for (int i = 1; i < 5; i++) {
            for(int j = 0; j< i; j++){
                System.out.print("*");
            }
            System.out.println();
        }

        System.out.println();
        System.out.println();

        int size = 5;
        for (int i = 0; i < size; i++) {
            for (int j = size; j > i ; j--) {
                System.out.print(" ");
            }
            for (int k = 0; k < i; k++) {
                System.out.print("*");
            }
            for (int h = 1; h < i; h++) {
                System.out.print("*");
            }
            System.out.println();
        }
    }
}
