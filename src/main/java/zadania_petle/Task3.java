package zadania_petle;

import java.util.Random;
import java.util.Scanner;
import static java.lang.Thread.sleep;


public class Task3 {
    public static void main(String[] args) throws InterruptedException {

        System.out.println("Let's play a game...");
        sleep(3000);
        System.out.println("Program losuje liczbę ;)");
        sleep(2000);
        System.out.println("Postaraj się ją zgadnąć z moimi podpowiedziami");


        Random random = new Random();
        int rand = random.nextInt(1000);
        int playerNumber = 0;
        Scanner in = new Scanner(System.in);


        do {
            System.out.println("Podaj swoją liczbę :");

            playerNumber = in.nextInt();

            if (playerNumber < rand) {
                System.out.println("Twoja podana liczba jest mniejsza od wylosowanej");
            } else if (playerNumber > rand) {
                System.out.println("Twoja podana liczba jest większa od wylosowanej");
            }

        } while (playerNumber != rand);


        System.out.println("Brawo! Udało ci się trafić!");

    }
}
