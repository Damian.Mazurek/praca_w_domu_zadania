package zadania_petle;

import java.util.Scanner;

public class Task12 {
    public static void main(String[] args) {

        System.out.println("Podaj słowo do odwrócenia: ");

        Scanner in = new Scanner(System.in);
        String word = in.nextLine();
        StringBuilder word2 = new StringBuilder();

        for (int i = word.length()-1; i >= 0 ; i--) {
            word2.append(word.charAt(i));
        }
        System.out.println(word2);
    }
}
