package zadania_string;

import java.util.Scanner;

public class Task2
{
    public static void main(String[] args)
    {
        System.out.println("Podaj słowo: ");
        Scanner in = new Scanner(System.in);
        String word = in.nextLine();

        System.out.println(word.contains("pies"));
    }
}
