package zadania_string;

import java.util.Scanner;

import static java.lang.Thread.sleep;

public class Task1
{
    public static void main(String[] args)
    {

        String name;
        try
        {
            System.out.println("Program sprawdza czy podane imię zaczyna się na literę J");
            sleep(2000);
        } catch (InterruptedException e)
        {

        }
        do
        {
            System.out.println("Podaj imię: ");

            Scanner in = new Scanner(System.in);
            name = in.next();
            System.out.println();

            System.out.println("Dane: " + "\n" + name);
            System.out.println("Wynik: " + "\n" + name.startsWith("J"));

            System.out.println();
        }
        while (!name.startsWith("J"));

    }
}
